import numpy as np

def check_symmetric(a, tol=1e-8):
    return np.allclose(a, a.T, atol=tol)

def return_row_vector_from_numpy_matrix(index, matrix):

    if len(matrix.shape) != 2:
        print("Error matrix is not 2d")
    else:
        row = matrix[index:index+1, :]

        return row

def return_row_array_from_numpy_matrix(index, matrix):

    if len(matrix.shape) != 2:
        print("Error matrix is not 2d")
    else:
        row = matrix[index, :]

        return row

def is_column_vector(vector):
    shape = vector.shape

    if shape[1] != 1:
        return False
    else:
        return True

def is_row_vector(vector):
    shape = vector.shape

    if shape[0] != 1:
        return False
    else:
        return True

def convert_array_to_row_vector(arr):
    
    dimension = arr.shape[0]

    arr.shape = (1,dimension)

    return arr
