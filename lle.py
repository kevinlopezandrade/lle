import sys
import matplotlib
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn as skl
import numpy as np
import utils
import scipy

from tensorflow.examples.tutorials.mnist import input_data
from sklearn.utils.validation import check_is_fitted

from sklearn.manifold import LocallyLinearEmbedding as SklearnLLE
from sklearn.neighbors import KDTree


mnist = input_data.read_data_sets("MNIST_data/", one_hot=False)


def plt_mnist(embedding, of="validation"):
    labels = getattr(mnist, of).labels
    
    if len(labels) != len(embedding):
        raise ValueError("Number of labels does not match number of embeddings")
    
    for label in range(10):
        idx = (labels == label)
        plt.scatter(embedding[idx, 0], embedding[idx, 1], alpha=0.5, label=str(label))
    plt.legend()
    plt.show()


def compute_k_neighbours(datum, k, tree):
    payload = [datum]
    query = tree.query(payload, k=k+1, return_distance=False)

    indexes = query[0][1:]

    if indexes.size != k: 
        raise ValueError("Error while computing the nearest_neightbours")

    # Indexes should be sorted before returning
    sorted_indexes = np.sort(indexes)

    return sorted_indexes


def compute_local_covariance_matrix(datum, X, nearest_neightbours_indexes):
    
    ndim = X.shape[1]
    k = nearest_neightbours_indexes.shape[0]

    nearest_neightbours = X[nearest_neightbours_indexes,:]

    row_datum = utils.convert_array_to_row_vector(datum)
    differences_matrix = nearest_neightbours - row_datum

    if differences_matrix.shape != (k, ndim):
        raise ValueError("Error while computing the differences_matrix")

    C = np.matmul(differences_matrix, differences_matrix.T)

    if C.shape != (k, k):
        raise ValueError("Error while computing the local_covariance_matrix")
    
    return C


def solve_linear_system(A):

    k = A.shape[0]
    b = np.ones(k)
    linear_system_solution = np.linalg.solve(A,b)
    
    rescale_factor = 1/(sum(linear_system_solution))
    normalized_solution  = rescale_factor*linear_system_solution
    
    if normalized_solution.shape[0] != k:
        raise ValueError("Wrong number of coefficients")
    
    return normalized_solution

def compute_coefficient_matrix(X, tree, k):
    nsamples = X.shape[0]
    ndim = X.shape[1]

    matrix_coefficients = np.zeros((nsamples, nsamples))

    for i in range(nsamples):

        datum = utils.return_row_array_from_numpy_matrix(i, X)
        nearest_neightbours_indexes = compute_k_neighbours(datum, k, tree)
        C = compute_local_covariance_matrix(datum, X, nearest_neightbours_indexes)
        coefficients = solve_linear_system(C)
        print("Solved linear system for vector {}".format(i))

        matrix_coefficients[i, nearest_neightbours_indexes] = coefficients

    return matrix_coefficients


def compute_embedded_vectors(W, N, d):
    I = np.identity(N)
    aux = I - W
    M = np.matmul(aux.T, aux)

    eigenvalues, matrix_eigenvectors = scipy.linalg.eigh(M, eigvals=(1,d), overwrite_a=True)
    indexes = np.argsort(np.abs(eigenvalues))

    error = np.sum(eigenvalues)
    matrix_of_solutions = matrix_eigenvectors[:, indexes]

    return (matrix_of_solutions, M, error)


def plot_grid_search_lle(X, interval=(2,10)):

    size = (interval[1] - interval[0]) + 1

    n_neighbors = list(range(interval[0], interval[1]+1))
    n_components = list(range(interval[0], interval[1]+1))
    
    grid_matrix = np.empty((size, size))

    print(grid_matrix.shape)

    for neighbors_index in range(len(n_neighbors)):
        for components_index in range(len(n_components)):
            # LLE = SklearnLLE(n_components=n_components[components_index], n_neighbors=n_neighbors[neighbors_index])
            LLE = LocallyLinearEmbedding(n_components=n_components[components_index], n_neighbors=n_neighbors[neighbors_index])
            LLE.fit(X)
            error = LLE.reconstruction_error_
            print(error)
            print(neighbors_index, components_index)

            grid_matrix[neighbors_index, components_index] = error
            

    # grid_matrix = np.load("./gridMatrix.npy")

    # Select only columns from 2 to 10

    grid_matrix = grid_matrix[:9, :9]

    fig, ax = plt.subplots()
    im = ax.imshow(grid_matrix)



    cbar = ax.figure.colorbar(im, ax=ax)

    ax.set_xticks(np.arange(len(n_components)))
    ax.set_yticks(np.arange(len(n_neighbors)))

    ax.set_xticklabels(n_components)
    ax.set_yticklabels(n_neighbors)

    ax.set_title("Grid search over neighbors and components")
    ax.set_ylabel("Neighbors")
    ax.set_xlabel("Components")

    for i in range(len(n_neighbors)):
        for j in range(len(n_components)):
            number_formated = np.format_float_scientific(grid_matrix[i, j], unique=True, precision=5)
            text = ax.text(j, i, number_formated,
                           ha="center", va="center", color="w")

    fig.tight_layout()
    plt.show()

def plot_matrix_M(M):
    labels = getattr(mnist, "validation").labels

    permutted_matrix = np.empty((M.shape[0], M.shape[1]))

    if len(labels) != len(M):
        raise ValueError("Number of labels does not match number of embeddings")

    jump = 0

    # Permute rows
    for label in range(10):
        idx = np.argwhere(labels == label)
        idx.shape = (len(idx), )
        sequence_indexes = np.array(range(len(idx)))
        new_indexes = jump + sequence_indexes

        for row_index in range(len(idx)):
            row = M[idx[row_index],:]
            nonzero_indexes = (row > 0)
            number_of_nonzero_elements = np.sum(nonzero_indexes)

            row[nonzero_indexes] = (label+1)*np.ones(number_of_nonzero_elements)

            permutted_matrix[new_indexes[row_index], :] = row

            # print("Repaiting vector : {}".format(new_indexes[row_index]))

        jump = new_indexes[-1] + 1
    

    jump = 0

    # Permut columns
    permutted_copy = np.copy(permutted_matrix)
    for label in range(10):
        idx = (labels == label)
        sequence_indexes = np.array(range(sum(idx)))
        new_indexes = jump + sequence_indexes

        permutted_matrix[:, new_indexes] = permutted_copy[:, idx]

        jump = new_indexes[-1] + 1

    plt.imshow(permutted_matrix)
    plt.show()


def plot_spectrum_M(M):
    eigenvalues = scipy.linalg.eigvalsh(M, overwrite_a=True)

    plt.plot(np.sort(eigenvalues))
    plt.title("Sorted eigenvalues")
    plt.show()



class LocallyLinearEmbedding(skl.base.BaseEstimator, skl.base.TransformerMixin):
    """Template class for LLE, compare to `sklearn LLE`_.
    
    Attributes:
        embedding_vectors_ (np.ndarray): Embedding of input X with shape (samples, n_components)
        nbrs_X (e.g. sklearn.neighbors.KDTree): NearestNeighbors (NN) object, stores NN for training set X.
        nbrs_y (e.g. sklearn.neighbors.KDTree): NearestNeighbors (NN) object, stores NN for embedding_vectors_.
        M_ (np.array): Symmetric matrix (samples, samples), used in quadratic form for embedding.
        X_ (np.array): Copy of training set with shape (samples, dim).
    
       .. _`sklearn LLE`: http://scikit-learn.org/stable/modules/generated/sklearn.manifold.LocallyLinearEmbedding.html
    """
    
    def __init__(self, n_components=2, n_neighbors=5):
        self.n_neighbors = n_neighbors
        self.n_components = n_components
        self.embedding_ = None
        self.nbrs_X = None
        self.X_ = None
        self.M_ = None
        self.coefficients_X = None
        self.nbrs_y = None
        self.reconstruction_error_ = None
    
    def fit(self, X):
        """Compute LLE embedding of vectors X
        
        First, compute nbrs_X and M_.
        Finally, compute embedding_vectors_.
        
        Args:
            X (np.ndarray): Input array with shape (samples, dim)
        
        Returns:
            self
        """

        nsamples = X.shape[0]
        ndim = X.shape[1]

        matrixCoefficients = np.zeros((nsamples, nsamples))

        tree = KDTree(X)

        # Uncomment the line below to save time
        # matrixCoefficients = np.load("./matrix_coef.npy")

        matrixCoefficients = compute_coefficient_matrix(X, tree, self.n_neighbors)

        embedded, M, error = compute_embedded_vectors(matrixCoefficients, nsamples, self.n_components)

        self.embedding_ = embedded
        self.nbrs_X = tree
        self.X_ = X
        self.coefficients_X = matrixCoefficients
        self.nbrs_y = KDTree(embedded)
        self.M_ = M
        self.reconstruction_error_ = error

    
    def transform(self, X):
        """Map new vectors X to embedding space
        
        Use the fitted model to map new vectors to the space with dimension n_components.
        
        Args:
            X (np.ndarray): Input array with shape (new_samples, dim)
            
        Returns:
            y (np.ndarray): Embedded vectors with shape (new_samples, n_components)
        """


        if len(X.shape) == 1:
            # Just one element
            X = utils.convert_array_to_row_vector(X)
            nsamples, ndim = X.shape
        else:
            nsamples, ndim = X.shape

        embedded_vectors = np.empty((nsamples, self.n_components))

        for i in range(nsamples):
            datum = utils.return_row_array_from_numpy_matrix(i, X)
            nearest_neightbours_indexes = compute_k_neighbours(datum, self.n_neighbors, self.nbrs_X)
            local_covariance_matrix = compute_local_covariance_matrix(datum, self.X_, nearest_neightbours_indexes) 
            coefficients = utils.convert_array_to_row_vector(solve_linear_system(local_covariance_matrix))

            nearest_neightbours = self.embedding_[nearest_neightbours_indexes]
            embedded_vectors[i] = np.dot(coefficients,nearest_neightbours)
            # print("Vector {} calculated: {}".format(i, np.sum(coefficients)))

        return embedded_vectors 
            
        


    def inverse_transform(self, y, k=5):
        """Map new vectors y to input space with dimension dim.
        
        Use the fitted model to map vectors y to the original input space.
        
        Args:
            y (np.ndarray): Input array with shape (new_samples, n_components)
            
        Returns:
            X (np.ndarray): Vectors with shape (new_samples, dim)
        """
        check_is_fitted(self, ["embedding_", "X_", "nbrs_y"])
        

        if len(y.shape) == 1:
            # Just one element
            y = utils.convert_array_to_row_vector(y)
            nsamples, ndim = y.shape
        else:
            nsamples, ndim = y.shape

        original_dimension = self.X_.shape[1]
        
        reconstructed_vectors = np.empty((nsamples, original_dimension))

        for i in range(nsamples):
            datum = utils.return_row_array_from_numpy_matrix(i, y)
            nearest_neightbours_indexes = compute_k_neighbours(datum, k, self.nbrs_y)

            
            plt.figure()
            rows = 1
            
            for j in range(nearest_neightbours_indexes.size):
                print(j)
                plt.subplot("{}{}{}".format(rows, k+1, j+1))
                plt.imshow(self.X_[nearest_neightbours_indexes[j]].reshape(28,28))
            
            # plt.show()

            nearest_neightbours_original_space = self.X_[nearest_neightbours_indexes]

            local_covariance_matrix = compute_local_covariance_matrix(datum, self.embedding_, nearest_neightbours_indexes)
            coefficients = utils.convert_array_to_row_vector(solve_linear_system(local_covariance_matrix))

            reconstructed_vectors[i] = np.dot(coefficients, nearest_neightbours_original_space)


        return reconstructed_vectors
