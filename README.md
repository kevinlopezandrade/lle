# Locally Linear Embedding

This repo contains the implementation of the non linear dimensionality
reduction techique **Locally Linear Embedding**. The implementation follows
the Sklearn API.


### References

* Nonlinear dimensionality reduction by locally linear embedding.
  Sam Roweis & Lawrence Saul.
  Science, v.290 no.5500 , Dec.22, 2000. pp.2323--2326.
